package com.indigo.main.modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;

public class AdapterTestCases implements Serializable{
	public AdapterTestCases() {
		
	}
	private static final long serialVersionUID = -1L;
	
	private int numberCaseT;
	private int consecutiveDigitsK;
	private int digitN;
	private String textDigit;
	private LinkedHashMap<String, Integer> mapProducts;
	private ArrayList<String> explanation = new ArrayList<String>();
	private int caseTest = 0;

	public int getNumberCaseT() {
		return numberCaseT;
	}
	public void setNumberCaseT(int numberCaseT) {
		this.numberCaseT = numberCaseT;
	}
	public int getConsecutiveDigitsK() {
		return consecutiveDigitsK;
	}
	public void setConsecutiveDigitsK(int consecutiveDigitsK) {
		this.consecutiveDigitsK = consecutiveDigitsK;
	}
	public int getDigitN() {
		return digitN;
	}
	public void setDigitN(int digitN) {
		this.digitN = digitN;
	}
	public String getTextDigit() {
		return textDigit;
	}
	public void setTextDigit(String textDigit) {
		this.textDigit = textDigit;
	}

	public LinkedHashMap<String, Integer> getMapProducts() {
		return mapProducts;
	}
	public void setMapProducts(LinkedHashMap<String, Integer> mapProducts) {
		this.mapProducts = mapProducts;
	}
	public ArrayList<String> getExplanation() {
		return explanation;
	}
	public void setExplanation(ArrayList<String> explanation) {
		this.explanation = explanation;
	}
	public int getCaseTest() {
		return caseTest;
	}
	public void setCaseTest(int caseTest) {
		this.caseTest = caseTest;
	}
}
