package com.indigo.main;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import com.indigo.main.control.ControlImpl;
import com.indigo.main.modelo.AdapterTestCases;

public class LargestProduct {
	
	public static void main(String Args[]) throws Exception{		
		ControlImpl ctrl = new ControlImpl(); 	
		InputStreamReader insert = new InputStreamReader(System.in);
		BufferedReader buffer = new BufferedReader(insert);
		
		//Insert value of T
		boolean valueTisOK = false;		
		while(!valueTisOK) { 
			System.out.println("Insert number of test case. Constraint: min 1, max 100");
			String inputText = buffer.readLine();
			valueTisOK = ctrl.insertValueofT(inputText);
			if(!valueTisOK)
				System.out.println("Please insert only a number min1, max 100. Try again...");					
		}
		System.out.println("_____________________________________________________________________");
		
		int countCases = 0;				
		//iterate test cases
		while(countCases < ctrl.getAdapter().getNumberCaseT()) { //Cases Test
			ctrl.setCaseAdapter(countCases+1);
			System.out.println("\nInsert the values N and K for the case: " + (countCases+1) + " Example: 10 5. Where 10 is N and 5 is K");
			System.out.println("Constraints: 1 <= [K] <= 7 and [K] <= [N] <= 1000.");
			boolean numbers = ctrl.getNumbers(buffer.readLine());		
			if(numbers) {
				boolean digitNumber = false;
				while(!digitNumber) {					
					System.out.println("Write a number with length N ["+ctrl.getAdapter().getDigitN()+" digits] ");
					digitNumber = ctrl.validateNumber(buffer.readLine(), ctrl.getAdapter().getDigitN());
					if(digitNumber)
						countCases++;
					else
						System.out.println("The length of the number must be equal to the value of N [" + ctrl.getAdapter().getDigitN() +"]");						
				}
				
				ctrl.getListProducts();
				ctrl.searchMax();
			}
		}
		
		//Result
		System.out.println("_____________________________________________________________________");
		System.out.println(" ..:: Maximus products for each case ::.. ");
		for(String messageExplanation: ctrl.getAdapter().getExplanation()) {
			System.out.println(messageExplanation);
		}						
	}	
}
