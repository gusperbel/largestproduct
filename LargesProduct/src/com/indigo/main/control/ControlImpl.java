package com.indigo.main.control;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.ArrayList;
import java.util.Map;

import com.indigo.main.modelo.AdapterTestCases;

public class ControlImpl{	
	private static AdapterTestCases adapterCases = new AdapterTestCases();
	
	//Recupera el valor insertado para T
	public boolean insertValueofT(String txtInput) {
		boolean resp = false;
		try {			
			int cases = Integer.parseInt(txtInput); 
			if(cases >= 1 && cases <= 100) {
				resp = true;
				adapterCases.setNumberCaseT(cases);
			}			
		}catch(Exception e) {
							
		}
		return resp;
	}
	
	//Recupera y valida cuando se inserta el valor de N y K
		public boolean getNumbers(String txtNumbers){
			boolean resp = false;
			try {						
				String[] separateNumbers = txtNumbers.trim().split(" ");			
				if(separateNumbers.length == 2) {
					int[] p = new int[separateNumbers.length];
					for(int x=0; x < separateNumbers.length; x++) {
						p[x] =  Integer.parseInt(separateNumbers[x]);
					}
					
					if((1 <= p[1] && p[1] <= 7) && 
							(p[1] <= p[0] && p[0] <= 1000)) {
						adapterCases.setConsecutiveDigitsK(p[1]);
						adapterCases.setDigitN(p[0]);
						resp = true;
					}else {
						System.out.println("The value of N, must be greater than T. Please try again...");
					}
					
				} else {
					System.out.println("Please insert only two numbers...");
				}
			}catch(Exception er) {
				System.out.println("Please insert only two numbers...");
			}
			return resp;						
		}
		
	//Genera la lista de numeros consecutivos a partir del valor K y realiza la multiplicacion de cada uno	
	public void getListProducts() {
		LinkedHashMap<String, Integer> products = new LinkedHashMap<String, Integer>();
		int start = 0;
		int end = adapterCases.getConsecutiveDigitsK();
		while( end <= adapterCases.getDigitN() ) {			
			String product = adapterCases.getTextDigit().substring(start, end);			
			
			int mult = 1;
			for(int x=0; x < product.length(); x++) {				
				mult = mult * Integer.parseInt(product.charAt(x)+"");			
			}
			products.put(product, mult);
			end++; start++;
		}
		adapterCases.setMapProducts(products);
	}
	
	//Busca el maximo producto y genera los mensajes de salida.
	public void searchMax() {
		LinkedHashMap<String, Integer> results = adapterCases.getMapProducts();
		String[] maxProduct = Collections.max(results.entrySet(), Map.Entry.comparingByValue()).toString().split("=");     
		StringBuilder message = new StringBuilder();
		
		if(Integer.parseInt(maxProduct[1]) != 0) {
			
			message.append("For "+adapterCases.getTextDigit()+" and selecting K = " + adapterCases.getConsecutiveDigitsK());		
			message.append(" consecutive digits, we have ");
			for (Map.Entry<String, Integer> entry : results.entrySet()) {
				message.append(entry.getKey()+", ");		    
			}
			message.append("\nWhere ");
			for(int x=0; x < maxProduct[0].length(); x++) {				
				if(x==0)
					message.append((maxProduct[0].charAt(x)+" "));
				else
					message.append(("x "+maxProduct[0].charAt(x)+" "));
			}
			message.append("gives maximum product as " + maxProduct[1]);
		}else {
			message.append("For "+adapterCases.getTextDigit()+", 0 lies in all selection of "+adapterCases.getConsecutiveDigitsK());
			message.append(" consecutive digits hence maximum product remains 0.");
		}
		
		ArrayList<String> explanation = adapterCases.getExplanation();
		explanation.add("\nCase "+adapterCases.getCaseTest()+" -> "+maxProduct[1]+"\n Explanation:");
		explanation.add(message.toString());
		adapterCases.setExplanation(explanation);
	}
		
	public boolean validateNumber(String textNumber, int lengthDigits) {
		boolean resp = false;
		if(textNumber.length() == lengthDigits) {
			resp = true;
			adapterCases.setTextDigit(textNumber);
		}
		return resp;
	}
	
	public AdapterTestCases getAdapter() {
		return this.adapterCases;
	}
	
	public void setCaseAdapter(int caseTest) {
		adapterCases.setCaseTest(caseTest);
	}
}
